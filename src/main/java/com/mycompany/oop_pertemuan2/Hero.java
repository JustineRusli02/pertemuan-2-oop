/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oop_pertemuan2;

import java.util.ArrayList;


/**
 *
 * @author windows-10
 */
public class Hero {
    
    private String name;
    ArrayList<Job> job = new ArrayList<>();
    
    public Hero(){
        
    }

    
    public Hero(String name, Job job) {
        this.job.add(job);
        this.name = name;
    }

    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Job> getJob() {
        return job;
    }

    public void setJob(ArrayList<Job> job) {
        this.job = job;
    }
    
    
    
    
    
    
}
