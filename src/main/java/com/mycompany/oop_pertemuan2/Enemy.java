/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oop_pertemuan2;

/**
 *
 * @author windows-10
 */
public class Enemy {
    private int hp;
    private int damage;

    public Enemy(int hp, int damage) {
        this.hp = hp;
        this.damage = damage;
    }
    
    

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
    
    
}
