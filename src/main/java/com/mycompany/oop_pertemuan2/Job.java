/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oop_pertemuan2;

/**
 *
 * @author windows-10
 */
public abstract class Job {
    private String name;
    private int hp, mp;
    private int damage;
    private String weapon;
    

    public Job(String name, int hp, int mp, int damage, String weapon) {
        this.name = name;
        this.hp = hp;
        this.mp = mp;
        this.damage = damage;
        this.weapon = weapon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    
    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }
    
    public void eat(){
        
    }
    
    public void run(){
        
    }
    
    public void walk(){
        
    }
    
    public void sleep(){
        
    }
    public void attack(){
        
    }
    
    public void getHit(){
        
    }
    
    public void heal(){
        
    }
}
