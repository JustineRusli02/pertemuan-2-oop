/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oop_pertemuan2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author windows-10
 */
public class Main {

    Scanner scan = new Scanner(System.in);

    public static void main(String args[]) {
        new Main();
    }

    public Main() {

        Hero hero = new Hero();
        int menu;

        do {
            System.out.println("Menu");
            System.out.println("1. Choose Job");
            System.out.println("2. Exit");
            System.out.print("Choose menu: ");
            menu = scan.nextInt();
        } while (menu < 1 || menu > 2);

        switch (menu) {
            case 1:
                int chooseJob = 0;
                String name;

                do {
                    System.out.println("1. Warrior");
                    System.out.println("2. Ranger");
                    System.out.println("3. Mage");
                    System.out.println("Choose Job: ");
                    chooseJob = scan.nextInt();
                } while (chooseJob < 1 || chooseJob > 3);

                System.out.print("Input Name: ");
                name = scan.nextLine();

                if (chooseJob == 1) {
                    hero = new Hero(name, new Warrior("Warrior", 200, 50, 30, "Sword"));

                } else if (chooseJob == 2) {
                    hero = new Hero(name, new Ranger("Ranger", 100, 200, 50, "LongBow"));
                } else if (chooseJob == 3) {
                    hero = new Hero(name, new Mage("Mage", 150, 300, 40, "Wizard Staff"));

                }

                System.out.println("Name: " + name);
                System.out.println("Job: " + hero.job.get(0).getName());
                System.out.println("HP: " + hero.job.get(0).getHp());
                System.out.println("MP: " + hero.job.get(0).getMp());
                System.out.println("Damage: " + hero.job.get(0).getDamage());
                System.out.println("Press enter to go to battle...");
                scan.nextLine();

                space();
                attack(hero);

                break;
            case 2:
                break;
        }

    }

    public void space() {
        for (int i = 0; i < 30; i++) {
            System.out.println(" ");
        }
    }

    public void attack(Hero hero) {
        Enemy enemy = new Enemy(200,10);
        int flag = 0;
        int choice;

        System.out.println("Enemy Hp: " + enemy.getHp());
        System.out.println("Enemy Damage: " + enemy.getDamage());
        System.out.println("");
        scan.nextLine();

        do {

            System.out.println("1. Attack");
            System.out.println("2. Heal");
            System.out.print("Choice: ");
            choice = scan.nextInt();

            if (choice == 1) {
                if (enemy.getHp() <= 0) {
                    System.out.println("You win!");
                    flag = 0;

                } else if (hero.job.get(0).getHp() <= 0) {
                    System.out.println("You lose!");
                    flag = 0;
                } else {

                    System.out.println("Hero attack: " + hero.job.get(0).getDamage());
                    if (enemy.getHp() - hero.job.get(0).getDamage() <= 0) {
                        enemy.setHp(0);
                        System.out.println("Enemy Hp: " + enemy.getHp());
                        System.out.println("Enemy is dead... you win");
                        break;

                    } else {
                        enemy.setHp(enemy.getHp() - hero.job.get(0).getDamage());
                    }

                    System.out.println("Enemy Hp: " + enemy.getHp());
                    System.out.println("");
                    scan.nextLine();

                    System.out.println("Enemy attack: " + enemy.getDamage());
                    if (hero.job.get(0).getHp() - enemy.getDamage() <= 0) {
                        hero.job.get(0).setHp(0);
                        System.out.println("Hero Hp: " + hero.job.get(0).getHp());
                        System.out.println("Hero is dead...  you lost");
                        break;
                    } else {
                        hero.job.get(0).setHp(hero.job.get(0).getHp() - enemy.getDamage());
                    }

                    System.out.println("Hero Hp: " + hero.job.get(0).getHp());
                    System.out.println("");
                    scan.nextLine();
                    flag++;
                }
            }else if(choice == 2){
                System.out.println("Healing... ");
                System.out.println("Hp Before: "  + hero.job.get(0).getHp());
                hero.job.get(0).heal();
                System.out.println("Hp Now: " + hero.job.get(0).getHp());
                flag++;
            }

        } while (flag > 0);
    }

}
